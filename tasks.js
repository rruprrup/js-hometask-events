'use strict';
/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
*/

let menuBtn = document.querySelector("#menuBtn");
let menuDiv = document.querySelector("#menu");

menuBtn.addEventListener("click", () => {
  if (!menuDiv.classList.contains("menu-opened")) {
    menuDiv.classList.add("menu-opened");
  } else {
    menuDiv.classList.remove("menu-opened");
  };
});

menuDiv.addEventListener("click", () => {
  menuDiv.classList.remove("menu-opened");
});

/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
*/

let field = document.querySelector("#field");
let movedBlock = document.getElementById("movedBlock");

field.addEventListener("click", (event) => {
  let fieldCoord = field.getBoundingClientRect();
  let blockCoord = movedBlock.getBoundingClientRect()

  let clickWidth = event.clientX - fieldCoord.x;
  let fieldWidthMax = fieldCoord.width - blockCoord.width;
  let clickHeight = event.clientY - fieldCoord.y;
  let fieldHeightMax = fieldCoord.height - blockCoord.height;

  if (clickWidth > fieldWidthMax) {
    movedBlock.style.transform = `translate(450px, ${event.clientY - fieldCoord.y}px)`
  }
  if (clickHeight > fieldHeightMax) {
    movedBlock.style.transform = `translate(${event.clientX - fieldCoord.x}px, 250px)`
  }
  if (clickWidth > fieldWidthMax && clickHeight > fieldHeightMax) {
    movedBlock.style.transform = `translate(450px, 250px)`
  }
  if (clickWidth < fieldWidthMax && clickHeight < fieldHeightMax) {
    movedBlock.style.transform = `translate(${event.clientX - fieldCoord.x}px, ${event.clientY - fieldCoord.y}px)`
  }
});
// С вылезающим за пределы поля блоком было бы намного меньше кода :) Но я захотела ограничить перемещения блока полем.

/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
*/

let messages = document.querySelectorAll(".message");
messages.forEach(message => {
  message.addEventListener("click", (event => {
    let cross = event.target.closest("span");
    if (!cross) {
      return;
    }
    if (!message.contains(cross)) {
      return;
    }
    message.classList.add("remove");
  }));
});

/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
*/

let linksUl = document.querySelector("#links");
let links = linksUl.querySelectorAll("a");
links.forEach(link => {
  link.addEventListener("click", (event => {
    let address = link.getAttribute("href");
    if (!confirm(`Вы точно хотите перейти по этой ссылке: ${address} ?`)) {
      event.preventDefault();
    }
  }));
});

/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
*/

let input = document.querySelector("#fieldHeader");
let header = document.querySelector("#taskHeader");
input.oninput = function () {
  header.innerHTML = input.value;
}